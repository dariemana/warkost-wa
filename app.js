require('dotenv').config()
const { Client, MessageMedia } = require('whatsapp-web.js');
const express = require('express');
const bodyParser = require('body-parser');
const { body, validationResult } = require('express-validator');
const socketIO = require('socket.io');
const qrcode = require('qrcode');
const http = require('http');
const fs = require('fs');
const { phoneNumberFormatter } = require('./helpers/formatter');
const fileUpload = require('express-fileupload');
const axios = require('axios');
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })
const mysql = require('mysql');
const transporter = require('./helpers/sendmail');
let mailOption = {
  from: 'no-replay@wakost.com',
  to: 'darysp@gmail.com',
  subject: 'judul',
  text: 'isi'
};
const port = process.env.PORT || 8001;

const app = express();
app.use(bodyParser.json())
const server = http.createServer(app);
const io = socketIO(server);
const apikey = 'apikeyseru';
const con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(fileUpload({
  debug: true
}));

const SESSION_FILE_PATH = './whatsapp-session.json';
const LIST_NUMBER = './upload/output.json';
let sessionCfg;

const getSession = function(){
  if (fs.existsSync(SESSION_FILE_PATH)) {
    sessionCfg = require(SESSION_FILE_PATH);
  }

}

getSession();

const client = new Client({
  restartOnAuthFail: true,
  puppeteer: {
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--no-first-run',
      '--no-zygote',
      '--single-process', // <- this one doesn't works in Windows
      '--disable-gpu'
    ],
  },
  session: sessionCfg
});

const init = function (socket) {
  if (fs.existsSync(SESSION_FILE_PATH)) {
    if (socket) {
      socket.emit('message', 'Session sudah pernah ada');
      sessionCfg = require(SESSION_FILE_PATH);
    }else{
      sessionCfg = require(SESSION_FILE_PATH);
      client.on('ready', () => {
        console.log('wa ready')
      });
    
      client.on('authenticated', (session) => {
        console.log('AUTHENTICATED', session);
        sessionCfg = session;
        fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
          if (err) {
            console.error(err);
          }
        });
      });
    
      client.on('auth_failure', function (session) {
        console.log('message', 'Auth failure, restarting...');
      });
    
      client.on('disconnected', (reason) => {
        console.log('message', 'Whatsapp is disconnected!');
        fs.unlinkSync(SESSION_FILE_PATH, function (err) {
          if (err) return console.log(err);
          console.log('Session file deleted!');
        });
        client.destroy();
        client.initialize();
      });

    }
    

  } else {
    if (socket) {
      socket.emit('message', 'Connecting...');
    }
  }

}

init();

app.get('/', (req, res) => {
  res.sendFile('index.html', {
    root: __dirname
  });
});

app.get('/download', function (req, res) {
  const file = `${__dirname}/uploads/nomor.csv`;
  res.download(file); // Set disposition and send it.
});



client.on('message', msg => {

  if (msg.body == '!ping') {
    console.log('id', msg.id);
    msg.reply('pong');
  } else if (msg.body == 'good morning') {
    msg.reply('selamat pagi');
  } else if (msg.body == 'survey') {
    msg.reply('Apakah aplikasi ini bermanfaat buat anda ? jawab YA atau TIDAK');
  } else if (msg.body.toUpperCase() == 'YA') {
    msg.reply('selamat jawab anda *ya*, terima kasih telah mengikuti survey');
  } else if (msg.body.toUpperCase() == 'TIDAK') {
    msg.reply('selamat jawab anda *tidak*, terima kasih telah mengikuti survey');
  } else if (msg.body.toUpperCase() == 'DAFTAR') {
    msg.reply('silahkan masukan nama dan alamat anda dengan format "#Heru#surakarta"');
  } else if (msg.body.match(/#/g) == '#,#') {
    msg.reply('pendaftaran anda berhasil');
  } else if (msg.body.toUpperCase() == 'PESAN') {
    msg.reply('Silahkan pilih menu berikut 1.soto 2. mie ayam 3. bakso ketik angka menu yg dipesan ?');
  } else if (msg.body == '1') {
    msg.reply('Pesanan Soto anda segera disiapakan');
  } else if (msg.body == '2') {
    msg.reply('Pesanan Mie Ayam anda segera disiapakan');
  } else if (msg.body == '3') {
    msg.reply('Pesanan Bakso anda segera disiapakan');
  } else if (msg.body == '!groups') {
    client.getChats().then(chats => {
      const groups = chats.filter(chat => chat.isGroup);

      if (groups.length == 0) {
        msg.reply('You have no group yet.');
      } else {
        let replyMsg = '*YOUR GROUPS*\n\n';
        groups.forEach((group, i) => {
          replyMsg += `ID: ${group.id._serialized}\nName: ${group.name}\n\n`;
        });
        replyMsg += '_You can use the group id to send a message to the group._'
        msg.reply(replyMsg);
      }
    });
  }
});

client.initialize();

// Socket IO
io.on('connection', function (socket) {
  init(socket);

  client.on('qr', (qr) => {
    console.log('QR RECEIVED', qr);
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit('qr', url);
      socket.emit('message', 'QR Code received, silahkan scan barcode!');
    });
  });

  client.on('ready', () => {
    socket.emit('ready', 'Whatsapp is ready!');
    socket.emit('message', 'Whatsapp is ready!');
  });

  client.on('authenticated', (session) => {
    socket.emit('authenticated', 'Whatsapp is authenticated!');
    socket.emit('message', 'Whatsapp is authenticated!');
    console.log('AUTHENTICATED', session);
    sessionCfg = session;
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on('auth_failure', function (session) {
    socket.emit('message', 'Auth failure, restarting...');
  });

  client.on('disconnected', (reason) => {
    socket.emit('message', 'Whatsapp is disconnected!');
    fs.unlinkSync(SESSION_FILE_PATH, function (err) {
      if (err) return console.log(err);
      console.log('Session file deleted!');
    });
    client.destroy();
    client.initialize();
  });
});


const checkRegisteredNumber = async function (number) {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
}
//send mail
app.post('/send-mail',[
  body('email').notEmpty(),
  body('subject').notEmpty(),
  body('isi').notEmpty(),
],
async (req,res)=>{
  mailOption.subject =req.body.subject;
    mailOption.to =req.body.email;
    mailOption.text =req.body.isi;
    await transporter.sendMail(mailOption, function (error, info) {
      if (error) {
        res.status(412).json({
          status: false,
          message : "gagal"+info
        })
      } else {
        res.status(200).json({
          status: true,
          message : "send"
        })
      }
    });
});

// Send message
app.post('/send-message', [
  body('nomor').notEmpty(),
  body('pesan').notEmpty(),
], async (req, res) => {
  const errors = validationResult(req).formatWith(({
    msg
  }) => {
    return msg;
  });

  if (req.headers['api-key'] != apikey) {
    return res.status(401).json({
      status: false,
      message: 'api key tidak sesuai'
    })
  }

  if (!errors.isEmpty()) {
    return res.status(422).json({
      status: false,
      message: errors.mapped()
    });
  }

  const number = phoneNumberFormatter(req.body.nomor);
  const message = req.body.pesan;

  const isRegisteredNumber = await checkRegisteredNumber(number);

  if (!isRegisteredNumber) {
    return res.status(422).json({
      status: false,
      message: 'The number is not registered'
    });
  }

  client.sendMessage(number, message).then(response => {
    con.query(`INSERT INTO historis (nomor,pesan) values('${number}','${message}')`, function (err, result) {
      if (err) throw err;
      console.log("Result: " , result);
    })
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

// Send media
app.post('/send-media', async (req, res) => {
  const number = phoneNumberFormatter(req.body.number);
  const caption = req.body.caption;
  const fileUrl = req.body.file;

  if (req.headers['api-key'] != apikey) {
    return res.status(401).json({
      status: false,
      message: 'api key tidak sesuai'
    })
  }

  // const media = MessageMedia.fromFilePath('./image-example.png');
  // const file = req.files.file;
  // const media = new MessageMedia(file.mimetype, file.data.toString('base64'), file.name);
  let mimetype;
  const attachment = await axios.get(fileUrl, {
    responseType: 'arraybuffer'
  }).then(response => {
    mimetype = response.headers['content-type'];
    return response.data.toString('base64');
  });

  const media = new MessageMedia(mimetype, attachment, 'Media');

  client.sendMessage(number, media, {
    caption: caption
  }).then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

const findGroupByName = async function (name) {
  const group = await client.getChats().then(chats => {
    return chats.find(chat =>
      chat.isGroup && chat.name.toLowerCase() == name.toLowerCase()
    );
  });
  return group;
}

app.post('/upload', function (req, res) {
  let sampleFile;
  let uploadPath;
  let result = [];

  if (req.headers['api-key'] != apikey) {
    return res.status(401).json({
      status: false,
      message: 'api key tidak sesuai'
    })
  }

  if (!req.files || Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  console.log('req.files >>>', req.files); // eslint-disable-line

  sampleFile = req.files.sampleFile;

  uploadPath = __dirname + '/uploads/' + sampleFile.name;

  sampleFile.mv(uploadPath, function (err) {
    if (err) {
      return res.status(500).send(err);
    }
    nomor = fs.readFileSync(__dirname + '/uploads/nomor.csv');
    var array = nomor.toString().split("\r");
    console.log('array', array);
    for (let i = 0; i < array.length - 1; i++) {
      if (i != 0) {
        result.push(array[i].replace('\n', ''));
      }

    }
    let json = JSON.stringify(result);
    fs.writeFileSync(__dirname + '/uploads/output.json', json);
    res.send('Berhasil diupload ' + uploadPath);
  });
});


// Send message to group
// You can use chatID or group name, yea!
app.post('/send-group-message', [
  body('id').custom((value, { req }) => {
    if (req.headers['api-key'] != apikey) {
      return res.status(401).json({
        status: false,
        message: 'api key tidak sesuai'
      })
    }

    if (!value && !req.body.name) {
      throw new Error('Invalid value, you can use `id` or `name`');
    }
    return true;
  }),
  body('message').notEmpty(),
], async (req, res) => {
  const errors = validationResult(req).formatWith(({
    msg
  }) => {
    return msg;
  });

  if (!errors.isEmpty()) {
    return res.status(422).json({
      status: false,
      message: errors.mapped()
    });
  }

  let chatId = req.body.id;
  const groupName = req.body.name;
  const message = req.body.message;

  // Find the group by name
  if (!chatId) {
    const group = await findGroupByName(groupName);
    if (!group) {
      return res.status(422).json({
        status: false,
        message: 'No group found with name: ' + groupName
      });
    }
    chatId = group.id._serialized;
  }

  client.sendMessage(chatId, message).then(response => {
    res.status(200).json({
      status: true,
      response: response
    });
  }).catch(err => {
    res.status(500).json({
      status: false,
      response: err
    });
  });
});

server.listen(port, function () {
  console.log('App running on *: ' + port);
});
