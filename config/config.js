const mailOptions = {
    from: 'hendro@beltosolution.com',
    to: 'darysp@gmail.com',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
};

const smtpOption = {
    service: process.env.MAIL_SERVICE,
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SECURE,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
}

exports.modules = {
    mailOptions,smtpOption
}